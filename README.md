# Yuwen_IDS721_individual2

[![pipeline status](https://gitlab.com/candice1121/yuwen_ids721_individual2/badges/main/pipeline.svg)](https://gitlab.com/candice1121/yuwen_ids721_individual2/-/commits/main)

[Demo Youtube Link](https://youtu.be/CCyFm4vJ_dM)

This project implements a simple Web Service in Rust with Actix, containerized using Docker for seamless deployment. It includes CI/CD pipeline files to automate the docker image running, ensuring efficient continuous delivery of the Rust microservice.

## Guess Number Game Functionality
This web service implements a guess number game. It provides two endpoints:

1. Root Endpoint: This endpoint greets users and provides instructions on how to use the service.

2. `/guess` Endpoint: This endpoint allows users to guess a number. The service compares the guessed number with the target number and returns a JSON response indicating whether the guess was correct or not.

Screenshots from game:
![functionality](imgs/game.png)
![functionality](imgs/game_fail.png)

## Rust Actix Functionality
This Rust microservice utilizes the Actix web framework to provide a simple REST API, offering two endpoints. The root endpoint greets users and provides instructions on using the service, while the `'/guess'` endpoint allows users to guess a number and return the result to front page. The service efficiently handles requests by comparing the guessed number with the target number and returning a JSON response indicating whether the guess was correct or not. Powered by Actix web and built for seamless deployment using Docker, this microservice ensures reliable and scalable performance for guessing numbers via a user-friendly API.

## Containerization
This project is containerized using Docker for seamless deployment. Containerization allows for the creation of lightweight and portable environments, ensuring that the application runs consistently across different platforms and environments.

To containerize this project, follow these steps:

1. Install Docker on your machine if you haven't already. You can download Docker from the official website: [https://www.docker.com/get-started](https://www.docker.com/get-started).

2. Build the Docker image by running the following command in the project's root directory
```bash
sudo docker build
```
![docker_image](imgs/docker_image.png)

3. Deploy the image and check its functionality locally

Docker Container in Docker Desktop:
![docker_container](imgs/docker_container.png)

Local Deployment:
![local_deploy](imgs/local_deployment.png)

4. Create repo in DockerHub and Push image to DockerHub
![Dockerhub](imgs/dockerhub.png)

## Continuous Integration with GitLab
This project utilizes GitLab for continuous integration (CI). The CI process is defined in a `.gitlab-ci.yml` file in the root directory of the project. Here are the steps of my CI configuration:

![CI_pipeline](imgs/gitlab-ci-success.png)

```Stage Build```: The build task in this code snippet is a part of a CI/CD (Continuous Integration/Continuous Deployment) pipeline. It is responsible for building a Docker image for a Rust web application and pushing it to a container registry in gitlab.

```
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - cd rust-todo-list
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE
  after_script:
    - echo "Rust Web App Docker Image built and deployed successfully"
```
![stage-build](imgs/ci-build.png)

```Stage Run```: The run stage in this pipeline is responsible for pulling the Docker image from the GitLab Container Registry and running it as a Docker container. 
```
run:
  stage: run
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker pull $CI_REGISTRY_IMAGE
    - docker run -d -p 8000:8000 $CI_REGISTRY_IMAGE
  after_script:
    - echo "Rust Web App Docker Image running on port 8000 successfully"
```
![stage-run](imgs/ci-run.png)

