use actix_web::{web, App, HttpServer, Responder};
use actix_files as fs;
use rand::Rng;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Guess {
    guess: u32,
}

async fn guess(guess: web::Form<Guess>) -> impl Responder {
    let secret_number = rand::thread_rng().gen_range(1..=10);
    
    match guess.guess.cmp(&secret_number) {
        std::cmp::Ordering::Less => format!("Too low! The secret number is {}", secret_number),
        std::cmp::Ordering::Greater => format!("Too high! The secret number is {}", secret_number),
        std::cmp::Ordering::Equal => "Congratulations, you guessed the correct number!".to_string(),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {  
    HttpServer::new(|| {
        App::new()
            .route("/guess", web::post().to(guess))
            .service(fs::Files::new("/", "./static/").index_file("index.html"))
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await
}